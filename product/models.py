from flask.json import jsonify
from flask import request
from app import db, app
import uuid
from utils import img_upload


class Product:

    def add(self):
        if request.files and request.form:
            data = request.form['name']
            #img_upload function from utils
            img = img_upload(request)
            if data:
                product = {
                    "_id": uuid.uuid4().hex,
                    "name": data,
                    "image": img,
                }
                #check for empty data
                if data == '':
                    return jsonify({'error': "Product name cannot be empty!!"})
                
                elif not img:
                    return jsonify({'error': "Image field cannot be empty!!"})
                
                #check for existing products
                elif db.products.find_one({"name":product['name']}):
                    return jsonify({"error": "Product already exist!!" }), 400

                db.products.insert_one(product)

                return jsonify(product), 200

            return jsonify({"error": "please insert the data!!"})
        
        return jsonify({'error':"Data not found!!"}), 400

            # return jsonify({'error': 'image not found!!'})
    

    def all(self):
        data = db.products.find()
        if data:
            for product in data:
                return jsonify(product), 200
        return jsonify({"data": "No data found!!"})
    
    
    def findByID(self, id):
        data = db.products.find_one(
            {"_id": id}
        )
        return jsonify(data), 200
