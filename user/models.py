from datetime import datetime
from utils import generate_token
from flask.helpers import make_response
from flask.json import jsonify
from flask import request
from app import db, app
from werkzeug.security import generate_password_hash, check_password_hash
import uuid
import re


class User:

    def signup(self):
        
        data = request.get_json()
        if data:
            user = {
                "_id": uuid.uuid4().hex,
                "name": data['name'],
                "email": data['email'],
                "password": data['password']
            }
            email_regrex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
            # validating field for blank
            if user['email'] == '' or user['password'] == '' or user['name']=='':
                return jsonify({'error': "All Fields required!!"})
            
            #validating email
            elif (re.match(email_regrex, user['email'])):
                user['password'] = generate_password_hash(user['password'], method='sha256')

                #check for existing email address
                if db.users.find_one({"email":user['email']}):
                    return jsonify({"error": "Email address is already exist!!" }), 400
                # insert into database
                db.users.insert_one(user)
                #check the password for token generation
                if check_password_hash(user['password'], data['password']):
                    token = generate_token(user['_id'])
            else:
                return jsonify({'error': "Invalid Email!!"})

        return jsonify(user, {'token': token.decode('UTF-8')}), 200
    

    def login(self):
        
        data = request.get_json()
        # check for empty field
        if data['email'] == '' or data['password']=='':
            return jsonify({'error': 'username or password is empty!!'})

        elif not data or not data['email'] or not data['password']:
            return make_response('could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!!"'})
        
        # check for email in database
        user = db.users.find_one({
            'email': data['email']
        })

        # return jsonify(user)
        if not user:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!!"'})
        
        # check password for authentication
        if check_password_hash(user['password'], data['password']):
            token = generate_token(user['_id'])
            return jsonify({'token': token.decode('UTF-8')}, 200)

        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!!"'})

