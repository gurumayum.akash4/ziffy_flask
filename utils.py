from app import app, db
from flask import request, jsonify
import jwt
from functools import wraps
import datetime
import os
from werkzeug.utils import secure_filename
from flask.helpers import send_from_directory


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message' : 'Token is missing !!'}), 401
  
        try:
            # decoding the payload to fetch the stored details
            data = jwt.decode(token, app.config['SECRET_KEY'])
            print(data)
            current_user = db.users.find_one({
            '_id': data['id']
        })
        except:
            return jsonify({
                'message' : 'Token is invalid !!'
            }), 401
        # returns the current logged in users contex to the routes
        return  f(current_user, *args, **kwargs)
  
    return decorated


def generate_token(id):

    token = jwt.encode({'id': id, 
                                'exp': datetime.datetime.utcnow() 
                                + datetime.timedelta(minutes=30)}, 
                    app.config['SECRET_KEY'])
    return token


def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath


def img_upload(request):

    if request.files['image']:

        image_data = request.files['image']

        img_name = secure_filename(image_data.filename)
        create_new_folder(app.config['UPLOAD_FOLDER'])
        img_path = os.path.join(app.config['UPLOAD_FOLDER'], img_name)
        image_data.save(img_path)
        send_from_directory(app.config['UPLOAD_FOLDER'], img_name,  as_attachment=True)

        return img_path

    return jsonify({"error": "image not found"})