FROM python:3.6

ENV PYTHONDONTWRITEBYTECODE 1

ENV PYTHONUNBUFFERED 1

ADD . /app
WORKDIR /app

RUN apt-get update
RUN apt-get install -y apt-utils software-properties-common && apt-get update

RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir -p /app
COPY . /app

RUN chmod +x server

# CMD ["sh", "./server"]
# ENV FLASK_APP=app.py 
# ENV FLASK_ENV='development'

EXPOSE 5000
