import os
from flask import Flask
import pymongo
from pymongo.errors import ServerSelectionTimeoutError

app = Flask(__name__)

app.config['SECRET_KEY'] = 'thisissecret'
PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = '{}/uploads/'.format(PROJECT_HOME)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#Database Connection
client = pymongo.MongoClient('mongodb://172.30.0.3:27017/')
db = client.MongoDB

try:
    info = client.server_info()
except ServerSelectionTimeoutError:
    print("server is down")


#Routes
from user import routes
from product import routes


@app.route('/')
def home():
    # print(os.environ['ziffy_db_1_port_27017_tcp_addr'])
    return "<h1>Hello Flask</h1>"


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
